<?php wp_footer(); ?> 

<footer>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p class="small text-center">
                    <strong>Sextou!</strong> é uma propriedade de André Teixeira & Dico Didiraja | Desenvolvido por Didiraja Design
                </p>
            </div>
        </div> 
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="<?= get_template_directory_uri(); ?>/assets/js/plugin/slick/slick.min.js"></script>
<script src="<?= get_template_directory_uri(); ?>/assets/js/main.js"></script>

</body>
</html>