<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <link rel="icon" href="../../../../favicon.ico">

    <title>Sextou!</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,600i,700,700i,800" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    

    <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri(); ?>/assets/js/plugin/slick/slick.css"/>
    <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/assets/js/plugin/slick/slick-theme.css">

    <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/style.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/assets/css/main.css">

    <?php wp_head(); ?> 
</head>
<body>